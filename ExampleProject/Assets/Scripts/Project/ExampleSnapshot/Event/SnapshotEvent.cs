﻿using AgkCommons.Event;
using UnityEngine;

namespace Project.ExampleSnapshot.Event
{
    public class SnapshotEvent : GameEvent
    {
        public const string OBJECTS_CREATED = "objectsCreated";
        public SnapshotEvent(string name, GameObject target) : base(name, target)
        {
        }

        public SnapshotEvent(string name) : base(name)
        {
        }
    }
}