﻿using Adept.Logger;
using IoC.Attribute;
using RSG;
using UnityEngine;

namespace Project.ExampleSnapshot.Service
{
    public class SnapshotAutosaveConsumer : MonoBehaviour, IWorldServiceInitiable               
    {
        private static readonly IAdeptLogger _logger = LoggerFactory.GetLogger<SnapshotAutosaveConsumer>();
        
        private const int CHECK_TIMEOUT = 10;
        private const int SHOW_NOTIFICATION_TIMEOUT = 7;
        [Inject]                                                         
        private SnapshotAutosaveRepository _snapshotAutosaveRepository;  
        [Inject]
        private SnapshotNetService _snapshotNetService;
        [Inject]                                    
        private OverlayManager _overlayManager;     
        [Inject]
        private NotificationService _notificationService;
        [Inject]
        private SessionService _sessionService;
        private PromiseTimer _promiseTimer;
        public void Init()
        {
            _sessionService.AddListener<SessionStartEvent>(SessionStartEvent.STARTED, OnSessionStarted);
            _sessionService.AddListener<SessionFinishedEvent>(SessionFinishedEvent.FINISHED, OnSessionFinished);
        }
        private void Update()
        {
            _promiseTimer?.Update(Time.unscaledDeltaTime);
        }
        private void OnDestroy()
        {
            if (_sessionService.HasListener<SessionStartEvent>(SessionStartEvent.STARTED, OnSessionStarted)) {
                _sessionService.RemoveListener<SessionStartEvent>(SessionStartEvent.STARTED, OnSessionStarted);
            }
            if (_sessionService.HasListener<SessionFinishedEvent>(SessionFinishedEvent.FINISHED, OnSessionFinished)) {
                _sessionService.RemoveListener<SessionFinishedEvent>(SessionFinishedEvent.FINISHED, OnSessionFinished);
            }
        }
        private void OnSessionStarted(SessionStartEvent evt)
        {
            CheckCreatedSnapshot();
        }
        private void OnSessionFinished(SessionFinishedEvent evt)
        {
            _promiseTimer = null;
            TryHideThrobber();
           
        }
        private void CheckCreatedSnapshot(bool notificationShown = true)
        {
            _promiseTimer = new PromiseTimer();
            _promiseTimer.WaitFor(CHECK_TIMEOUT)
                         .Then(() => {
                             if (!_snapshotAutosaveRepository.Exists()) {
                                 CheckCreatedSnapshot();
                                 return;
                             }
                             SaveSnapshot(notificationShown);
                         })
                         .Done();
        }

        private void SaveSnapshot(bool notificationShown)
        {
            GameSnapshot gameSnapshot = _snapshotAutosaveRepository.Require();
            _snapshotNetService.Save(gameSnapshot)
                               .Then(onResolved: success => {
                                   if (!success) {
                                       _logger.Error("Save gameSnapshot error");
                                   }
                                   FinishSavingSnapshot(gameSnapshot);
                               })
                               .Catch(e => {
                                   if (e is NetworkAccessException) {
                                       HandleNetworkNotReachable(notificationShown);
                                       return;
                                   }
                                   _logger.Error("Save gameSnapshot error", e);
                                   FinishSavingSnapshot(gameSnapshot);
                               })
                               .Done();
        }
        private void FinishSavingSnapshot(GameSnapshot gameSnapshot)
        {
            if (!_snapshotAutosaveRepository.Exists()) {
                TryHideThrobber();
                CheckCreatedSnapshot();
                return;
            }
            if (gameSnapshot.Equals(_snapshotAutosaveRepository.Require())) {
                _snapshotAutosaveRepository.Delete();
                TryHideThrobber();
            }
            CheckCreatedSnapshot();
        }

        private void HandleNetworkNotReachable(bool notificationShown)
        {
            TryHideThrobber();
            if (notificationShown) {
                ShowNotification();
            }
            CheckCreatedSnapshot(false);
        }

        private void ShowNotification()
        {
            AlertNotification notification = AlertNotification
                                             .Create("#notification_autosaveNetworkNotReachable")
                                             .Controller(typeof(AlertNotificationPanel))
                                             .Timeout(SHOW_NOTIFICATION_TIMEOUT)
                                             .Get();
            _notificationService.ShowAlertNotification(notification);
        }

        private void TryHideThrobber()
        {
            if (!_overlayManager.IsThrobberShown()) {
                return;
            }
            _overlayManager.HideThrobber();
        }
    }
}