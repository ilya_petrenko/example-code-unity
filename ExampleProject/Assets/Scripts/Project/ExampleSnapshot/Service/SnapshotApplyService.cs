using System;
using System.Collections.Generic;
using System.Linq;
using Adept.Logger;
using AgkCommons.CodeStyle;
using FIMSpace.FOptimizing;
using IoC.Attribute;
using IoC.Util;
using RSG;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Project.ExampleSnapshot.Service
{
    [Injectable]
    public class SnapshotApplyService
    {
        private static readonly IAdeptLogger _logger = LoggerFactory.GetLogger<SnapshotApplyService>();
        [Inject]
        private IoCProvider<QuestManager> _questManager;
        [Inject]
        private IoCProvider<World> _world;
        [Inject]
        private IoCProvider<ObjectSnapshotMapper> _objectSnapshotMapper;
        [Inject]
        private IoCProvider<PlayerService> _playerService;
        [Inject]
        private SessionBuilder _sessionBuilder;
        [Inject]
        private IterationService _iterationService;
        [Inject]
        private SessionService _sessionService;
        [Inject]
        private SessionTimeService _sessionTimeService;

        public IPromise ApplySnapshot(GameSnapshot gameSnapshot)
        {
            GameSession gameSession = _sessionBuilder.BuildSnapshotSession(gameSnapshot.GameSessionSnapshot);
            return _sessionService.CreateSnapshotSession(gameSession)
                                  .Then(() => ApplyHouseSnapshot(gameSnapshot.HouseSnapshot))
                                  .Then(() => ApplyQuestsSnapshot(gameSnapshot.QuestSnapshots))
                                  .Then(() => _iterationService.StartSnapshotIteration())
                                  .Then(() => _playerService.Require().SetOptimizerCamera())
                                  .Then(() => ApplySessionTimeSnapshot(gameSnapshot.GameSessionSnapshot));

        }
        private void ApplyHouseSnapshot(HouseSnapshot houseSnapshot)
        {
            _world.Require().SetOptimizersNearState();
            
            foreach (ObjectSnapshot objectSnapshot in houseSnapshot.ObjectSnapshots) {
                IWorldObject worldObject = _world.Require().GetObjectBySnapshotId<IWorldObject>(objectSnapshot.SnapshotId);
                if (worldObject == null) {
                    _logger.Error("Not found gameObject by SnapshotId:= " + objectSnapshot.SnapshotId + " .Prefab name = " + objectSnapshot.Name);
                    continue;
                }
                try {
                    _objectSnapshotMapper.Require().ApplySnapshot(worldObject, objectSnapshot);
                } catch (Exception e) {
                    _logger.Error("Can't apply snapshot, object name= " + objectSnapshot.Name + ", objectSnapshotId= " + objectSnapshot.SnapshotId + ", objectPrefabId= " + objectSnapshot.PrefabId + ", parentGameObjectName= " + objectSnapshot.ParentGameObjectName,
                                  e);
                }
            }
            _world.Require().RecacheComponents();
        }
        
        private void DestroyGameObject(GameObject gameObject)
        {
            OptimizersManager.DetachDynamic(gameObject);
            _world.Require().RemoveController(gameObject.name);
            gameObject.SetActive(false);
            Object.Destroy(gameObject);
        }

        private void ApplyQuestsSnapshot(List<QuestSnapshot> questSnapshots)
        {
            foreach (Quest quest in _questManager.Require().Quests) {

                QuestSnapshot questSnapshot = questSnapshots.FirstOrDefault(q => q.QuestType == quest.QuestType.GetName());
                if (questSnapshot == null) {
                    _logger.Error("Can't find questType in questSnapshots. QuestType= " + quest.QuestType);
                    continue;
                }
                if (questSnapshot.Completed) {
                    quest.Complete();
                }
                foreach (QuestStep questStep in quest.Steps) {
                    QuestStepSnapshot questStepSnapshot = questSnapshot.QuestStepSnapshots.FirstOrDefault(s => questStep.TargetId == s.TargetId &&
                                                                                                               questStep.TargetState.GetName() == s.TargetState);
                    if (questStepSnapshot == null) {
                        _logger.Error("Can't find targetId in questStepSnapshot. TargetId= " + questStep.TargetId + ". QuestType= " + quest.QuestType);
                        continue;
                    }
                    if (!questStepSnapshot.Completed) {
                        continue;
                    }
                    questStep.Complete();
                }
            }
        }

        private void ApplySessionTimeSnapshot(GameSessionSnapshot gameSessionSnapshot)
        {
            _sessionTimeService.SessionStartTime -= gameSessionSnapshot.SessionDuration;
            _sessionTimeService.IterationStartTime -= gameSessionSnapshot.IterationDuration;
        }


    }
}