using AgkCommons.CodeStyle;
using GameKit.Repository;

namespace Project.ExampleSnapshot.Service
{
    [Injectable]
    public class SnapshotRepository : SingleModelRepository<GameSnapshot>
    {
    }
}