using Adept.Logger;
using AgkCommons.CodeStyle;
using AgkCommons.Resources;
using IoC.Attribute;
using JetBrains.Annotations;
using RSG;

namespace Project.ExampleSnapshot.Service
{
    [Injectable]
    public class SnapshotNetService : INetInitializable
    {
        private static readonly IAdeptLogger _logger = LoggerFactory.GetLogger<SnapshotNetService>();
        [Inject]                                                         
        private SnapshotAutosaveRepository _snapshotAutosaveRepository;  
        [Inject]
        private SnapshotEndpoint _snapshotEndpoint;       
        [Inject]
        private SnapshotRepository _snapshotRepository;
        [Inject]
        private ResourceService _resourceService;
        [Inject]
        private NetworkAccessService _networkAccessService;
        public IPromise Initialize()
        {
            return HasSaved()
                   .Then(saved => !saved ? Promise.Resolved() : InitSnapshotModel())
                   .Catch(e => {
                       _logger.Error("Initialize SnapshotService error", e);
                   });
        }
        
        [PublicAPI]
        public IPromise<bool> Save(GameSnapshot gameSnapshot)
        {
            if (!_networkAccessService.IsInternetReachability()) {
                return Promise<bool>.Rejected(new NetworkAccessException("Network not reachable"));
            }
            _logger.Info("Start save gameSnapshot in server");
            return _snapshotEndpoint.Save(gameSnapshot)
                                    .Then(b => {
                                        _logger.Info("Save gameSnapshot in server completed success");
                                        _snapshotRepository.Set(gameSnapshot);
                                        return b;
                                    })
                                    .Catch(e => {
                                        _logger.Error("Save gameSnapshot error", e);
                                        throw e;
                                    });
        }
        [PublicAPI]
        public IPromise<GameSnapshot> Load()
        {
            if (!_networkAccessService.IsInternetReachability()) {
                return Promise<GameSnapshot>.Rejected(new NetworkAccessException("Network not reachable"));
            }
            _logger.Info("Start load gameSnapshot from server");
            return _snapshotEndpoint.Load()
                                    .Then(snapshot => {
                                        _logger.Info("Load gameSnapshot from server completed success");
                                        _snapshotRepository.Set(snapshot);
                                        return snapshot;
                                    })
                                    .Catch(e => {
                                        _logger.Error("Load gameSnapshot error", e);
                                        throw e;
                                    });
        }
        [PublicAPI]
        public IPromise Delete()
        {
            _snapshotRepository.Delete();
            _snapshotAutosaveRepository.Delete();
            _logger.Info("Start Delete  gameSnapshot");
            return HasSaved()
                   .Then(saved => !saved ? Promise.Resolved() : DoDelete())
                   .Catch(e => {
                       _logger.Error("Delete gameSnapshot error", e);
                   });
                    
        }
        [PublicAPI]
        public IPromise<bool> HasSaved()
        {
            return _snapshotEndpoint.HasSaved();
        }
        [PublicAPI]
        public bool ExistsSnapshot()
        {
            return _snapshotRepository.Exists();
        }
      
        private IPromise DoDelete()
        {
            return _snapshotEndpoint.Delete()
                                    .Then(success => {
                                        if (!success) {
                                            _logger.Error("Delete gameSnapshot error");
                                        }
                                    });

        }
        
        private IPromise InitSnapshotModel()
        {
            return _snapshotEndpoint.Load()
                                    .Then(snapshot => {
                                        return CheckVersion(snapshot)
                                                .Then(s => {
                                                    if (s) {
                                                        _snapshotRepository.Set(snapshot);
                                                    }
                                                });
                                    });
        }
        private IPromise<bool> CheckVersion(GameSnapshot gameSnapshot)
        {
            return _resourceService.LoadResource<PrefabHouseModel>(gameSnapshot.GameSessionSnapshot.Prefab).Then(p => gameSnapshot.Version == p.Version);
        }
        
    }
}