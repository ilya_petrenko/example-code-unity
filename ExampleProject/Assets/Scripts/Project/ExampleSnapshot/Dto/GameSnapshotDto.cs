﻿namespace Project.ExampleSnapshot.Dto
{
    public class GameSnapshotDto
    {
        public byte[] Object { get; set; }
        
        public GameSnapshotDto(byte[] ob) {
            Object = ob;
        }
    }
}