using AgkCommons.CodeStyle;
using GameKit.Serialization;
using IoC.Attribute;
using RSG;
using WebApi;

namespace Project.ExampleSnapshot.Api
{
    [Injectable]
    public class SnapshotEndpoint
    {
        private const int MAX_RETRY_COUNT = 5;
        [Inject]
        private ApiClient _client;
        
        public IPromise<bool> Save(GameSnapshot gameSnapshot)
        {
            GameSnapshotDto gameSnapshotDto = new GameSnapshotDto(TransferObjectSerializer.SerializeObject(gameSnapshot));
            ApiRequest request = ApiRequest.Create("snapshot/save")
                                           .AddParam("gameSnapshotDto", gameSnapshotDto)
                                           .MaxRetryCount(MAX_RETRY_COUNT)
                                           .Build();
            return _client.Request<bool>(request);
        }
        
        public IPromise<GameSnapshot> Load()
        {
            ApiRequest request = ApiRequest.Create("snapshot/load")
                                           .MaxRetryCount(MAX_RETRY_COUNT)
                                           .Build();
            
            return _client.Request<GameSnapshotDto>(request)
                          .Then(d => TransferObjectSerializer.DeserializeObject<GameSnapshot>(d.Object));
        }
        public IPromise<bool> Delete()
        {
            ApiRequest request = ApiRequest.Create("snapshot/delete")
                                           .MaxRetryCount(MAX_RETRY_COUNT)
                                           .Build();
            return _client.Request<bool>(request);
        }
        public IPromise<bool> HasSaved()
        {
            ApiRequest request = ApiRequest.Create("snapshot/hasSaved")
                                           .MaxRetryCount(MAX_RETRY_COUNT)
                                           .Build();
            return _client.Request<bool>(request);
        }
    }
}