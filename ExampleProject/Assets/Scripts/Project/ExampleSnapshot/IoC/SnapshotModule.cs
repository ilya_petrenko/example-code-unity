using IoC.Api;

namespace Project.ExampleSnapshot.IoC
{
    public class SnapshotModule : IIoCModule
    {
        public void Configure(IIoCContainer container)
        {
            container.RegisterSingleton<SnapshotApplyService>();
            container.RegisterSingleton<SnapshotRepository>();
            container.RegisterSingleton<SnapshotNetService>();
        }
    }
}