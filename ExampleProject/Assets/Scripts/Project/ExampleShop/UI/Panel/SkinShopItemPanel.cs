﻿using AgkUI.Binding.Attributes;
using IoC.Attribute;
using RSG;

namespace Project.ExampleShop.UI.Panel
{
    [UIController(PREFAB)]
    public class SkinShopItemPanel : ShopItemPanel
    {
        private const string PREFAB = "UI/Shop/Panel/pfShopItemPanel@embeded";
        [Inject]
        private ShopService _shopService;
        [Inject]
        private SkinService _skinService;
        
        private void Start()
        {
            _skinService.AddListener<MonsterEvent>(MonsterEvent.UPDATED, OnSkinUpdated);
        }
        private void OnDestroy()
        {
            if (_skinService.HasListener<MonsterEvent>(MonsterEvent.UPDATED, OnSkinUpdated)) {
                _skinService.RemoveListener<MonsterEvent>(MonsterEvent.UPDATED, OnSkinUpdated);
            }
        }
        protected override void OnChangeClick(bool applied)
        {
            _overlayManager.LockScreen();
            ChangeAppliedSkin(applied)
                    .Then(s => {
                        if (s) {
                            _overlayManager.UnlockScreen();
                            return;
                        }
                        HandleErrorChangeAppliedItem(applied, null);
                    })
                    .Catch(e => { HandleErrorChangeAppliedItem(applied, e); });
        }
        protected override IPromise<string> Buy()
        {
            return _shopService.BuySkin(_descriptor.Id);
        }
        private IPromise<bool> ChangeAppliedSkin(bool applied)
        {
            return applied ? _skinService.ChangeSkin(_descriptor.Id) : _skinService.SetDefaultSkin();
        }
        private void OnSkinUpdated(MonsterEvent evn)
        {
            Applied = IsApplied();
        }

        protected override bool IsApplied()
        {
            return _skinService.HasSkin(_descriptor.Id);
        }
    }
}