﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Adept.Logger;
using AgkCommons.L10n.Service;
using AgkUI.Binding.Attributes;
using AgkUI.Binding.Attributes.Method;
using AgkUI.Binding.Field;
using AgkUI.Dialog.Service;
using AgkUI.Element.Buttons;
using AgkUI.Element.Images;
using IoC.Attribute;
using IoC.Util;
using JetBrains.Annotations;
using RSG;
using UnityEngine;

namespace Project.ExampleShop.UI.Panel
{
    public abstract class ShopItemPanel : MonoBehaviour
    {
        private static readonly IAdeptLogger _logger = LoggerFactory.GetLogger<ShopItemPanel>();
        private const string TOGGLE_APPLY_ITEM = "ToggleApplyItem";
        private const string PRICE_BUTTON = "PriceButton";
        
        [UITextBinding("ItemTitle")]
        private TextBinding _itemTitleLabel;
        [UIObjectBinding("ItemTitle")]
        private GameObject _itemTitle;
        [UIComponentBinding("ItemIcon")]
        private IconElement _itemIcon;
        [UITextBinding("Description")]
        private TextBinding _descriptionLabel;
        [UIObjectBinding("Description")]
        private GameObject _description;
        [UIObjectBinding("NotBought")]
        private GameObject _notBoughtContainer;
        [UITextBinding("DescriptionLock")]
        private TextBinding _descriptionLockLabel;   
        [UIObjectBinding("DescriptionLock")]
        private GameObject _descriptionLockContainer;   
        [UIObjectBinding("Lock")]
        private GameObject _lockContainer;
        [UIComponentBinding(TOGGLE_APPLY_ITEM)]
        private ToggleButton _toggleApplyItem;
        [UIComponentBinding(PRICE_BUTTON)]
        private PriceButton _priceButton;
        [Inject]
        protected OverlayManager _overlayManager;
        [Inject]
        private NotificationService _notificationService;
        [Inject]
        private L10nService _l10nService;
        [Inject]
        private ConditionService _conditionService;
        [Inject]
        private MoneyService _moneyService;
        [Inject]
        private DialogService _dialogService;
        [Inject]
        private InventoryService _inventoryService;
        [Inject]
        private IoCProvider<DialogManager> _dialogManager;

        protected ShopItemDescriptor _descriptor;

        private string _descriptionLock;
        private bool _bought;
        
        [UICreated]
        private void Init(ShopItemDescriptor descriptor)
        {
            _descriptor = descriptor;
            gameObject.name = descriptor.Id;
            SetButtonPrice(descriptor.Price);
            Title = descriptor.Name;
            _itemIcon.Icon = descriptor.Icon;
            Description = descriptor.Description;
            _descriptionLock = descriptor.DescriptionLock;
            Bought = IsBough();
            Applied = IsApplied();
            Availability = IsAvailability(descriptor.Availability);
            _priceButton.Interactable = true;
            Visibility = IsVisibility(descriptor.Visibility);
      
        }
        
        private bool CheckMoney()
        {
            if (_moneyService.HasMoney(_descriptor.Price)) {
                return true;
            }
            _notificationService.ShowAlertNotification(GetMoneyNotification());
            _dialogManager.Require().ShowModal<ShopDialog>(ShopDialog.BUTTONS_TAB, true);
         
            return false;
        }
        
        private void OnBuyClick()
        {
            if (!CheckMoney()) {
                return;
            }
            _overlayManager.LockScreen();
            Interactable = false;
            Buy().Then((id) => {
                     Bought = true;
                     _notificationService.ShowAlertNotification(GetBuyNotification());
                 })
                 .Catch(e => {
                     _overlayManager.UnlockScreen();
                     _logger.Error("Buy error itemId= " + _descriptor.Id, e);
                     _dialogService.ShowDefaultError("#purchaseException_error");
                 })
                 .Done(() => {
                     _overlayManager.UnlockScreen();
                     Interactable = true;
                 });
        }

        private AlertNotification GetMoneyNotification()
        {
            return AlertNotification.Create("#notification_notEnoughMoney").Controller(typeof(MoneyNotificationPanel)).Timeout(5).Get();
        }
        private AlertNotification GetBuyNotification()
        {
            return AlertNotification.Create("#notification_" + _descriptor.Type.GetName() + "Buy").Controller(typeof(AlertNotificationPanel)).Timeout(5).Get();
        }

        protected void HandleErrorChangeAppliedItem(bool applied, [CanBeNull] Exception e)
        {
            _logger.Error("can't change applied in itemId=" + _descriptor.Id, e);
            Applied = !applied;
            _overlayManager.UnlockScreen();
            _dialogService.ShowDefaultError("#dlgShop_appliedChangeError");
        }
        private void SetButtonPrice(int price)
        {
            _priceButton.Price = price.ToString(CultureInfo.InvariantCulture);
            _priceButton.SetCurrency(PriceButton.CurrencyType.BUTTONS, null);

        }
        private bool IsVisibility([CanBeNull] VisibilityDescriptor visibility)
        {
            return visibility == null || IsAllowedConditions(visibility.Conditions) || IsBough();
        }
        private bool IsAvailability([CanBeNull] AvailabilityDescriptor availability)
        {
            return availability == null || IsAllowedConditions(availability.Conditions) || IsBough();
        }
        private bool IsAllowedConditions([CanBeNull] List<ConditionDescriptor> conditions)
        {
            return conditions == null || _conditionService.CheckConditions(conditions, _descriptor.Id).IsAllowed();
        }
        private bool IsBough()
        {
            return _inventoryService.Inventory.HasItem(_descriptor.Id);
        }
        
        [UIOnClick(PRICE_BUTTON)]
        private void OnPriceClick()
        {
            OnBuyClick();
        }
        
        [UIOnClick(TOGGLE_APPLY_ITEM)]
        private void OnApplyItemClick()
        {
            OnChangeClick(_toggleApplyItem.IsOn);
        }
        protected abstract void OnChangeClick(bool applied);
        protected abstract IPromise<string> Buy();
        protected abstract bool IsApplied();
        protected bool Applied
        {
            get { return _toggleApplyItem.IsOn; }
            set
            {
                _toggleApplyItem.IsOn = value;
            }
            
        }
        private bool Interactable
        {
            set { _priceButton.Interactable = value; }
        }
        private bool Bought
        {
            set
            {
                _bought = value;
                _notBoughtContainer.SetActive(!value);
                _toggleApplyItem.gameObject.SetActive(value);
                _priceButton.gameObject.SetActive(!value);
            }
        }
        private string Title
        {
            set
            {
                if (string.IsNullOrEmpty(value)) {
                    _itemTitle.SetActive(false);
                    return;
                }

                _itemTitle.SetActive(true);
                _itemTitleLabel.Text = _l10nService.Message(value);
            }
        }
        private string Description
        {
            set
            {
                if (string.IsNullOrEmpty(value)) {
                    _description.SetActive(false);
                    return;
                }

                _description.SetActive(true);
                _descriptionLabel.Text = _l10nService.Message(value);
            }
        }
        private bool Visibility
        {
            set
            {
                gameObject.SetActive(value);
            }
        }
        
        private bool Availability
        {
            set
            {
                if (!string.IsNullOrEmpty(_descriptionLock)) {
                    _descriptionLockContainer.SetActive(!string.IsNullOrEmpty(_descriptionLock));
                    _descriptionLockLabel.Text = _l10nService.Message(_descriptionLock);
                }
                _lockContainer.SetActive(!value);
                if (!value) {
                    _notBoughtContainer.SetActive(false);
                } else {
                    _notBoughtContainer.SetActive(!_bought);

                }
            }
        }
    }
}