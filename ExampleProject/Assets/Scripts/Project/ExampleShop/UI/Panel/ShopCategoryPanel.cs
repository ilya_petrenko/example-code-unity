﻿using Adept.Logger;
using AgkCommons.Extension;
using AgkCommons.L10n.Service;
using AgkUI.Binding.Attributes;
using AgkUI.Binding.Field;
using AgkUI.Core.Model;
using AgkUI.Core.Service;
using IoC.Attribute;
using UnityEngine;

namespace Project.ExampleShop.UI.Panel
{
    [UIController(PREFAB)]
    public class ShopCategoryPanel : MonoBehaviour
    { 
        private static readonly IAdeptLogger _logger = LoggerFactory.GetLogger<ShopCategoryPanel>();
        private const string PREFAB = "UI/Shop/Panel/pfShopCategoryPanel@embeded";
        
        [UIObjectBinding("ItemScroll")]
        private GameObject _itemScroll;
        [UITextBinding("ErrorLabel")]
        private TextBinding _errorLabel;
        [Inject]
        private UIService _uiService;
        [Inject]
        private L10nService _l10nService;
        
        private ShopCategoryDescriptor _shopCategoryDescriptor;
     
        [UICreated]
        private void Init(ShopCategoryDescriptor shopCategoryDescriptor)
        {
            _itemScroll.gameObject.RemoveChildren();
            _shopCategoryDescriptor = shopCategoryDescriptor;
            CreateItemPanels();
            Configure();
        }
        private void CreateItemPanels()
        {
            foreach (ShopItemDescriptor shopItemDescriptor in _shopCategoryDescriptor.ShopItemDescriptors) {
                _uiService.Create(UiModel.Create(shopItemDescriptor.Type.GetShopPanel(), shopItemDescriptor).Container(_itemScroll))
                          .Catch(e => { 
                              _logger.Trace("Dialog was closed, itemScroll was destroyed", e);
                          })
                          .Done();
            }
        }
        private void Configure()
        {
            if (_shopCategoryDescriptor.ShopItemDescriptors.Count == 0) {
                _errorLabel.Text = _l10nService.Message("#dlgShop_" + gameObject.name + "ErrorLabel");
            }
        }
        
    }
}