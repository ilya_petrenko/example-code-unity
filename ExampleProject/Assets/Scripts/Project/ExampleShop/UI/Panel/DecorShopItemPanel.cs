﻿using AgkUI.Binding.Attributes;
using IoC.Attribute;
using RSG;

namespace Project.ExampleShop.UI.Panel
{
    [UIController(PREFAB)]
    public class DecorShopItemPanel : ShopItemPanel
    {
        private const string PREFAB = "UI/Shop/Panel/pfShopItemPanel@embeded";
        [Inject]
        private ShopService _shopService;
        [Inject]
        private RoomService _roomService;

        private void Start()
        {
            _roomService.AddListener<RoomEvent>(RoomEvent.UPDATED, OnRoomUpdated);
        }
        
        private void OnDestroy()
        {
            if (_roomService.HasListener<RoomEvent>(RoomEvent.UPDATED, OnRoomUpdated)) {
                _roomService.RemoveListener<RoomEvent>(RoomEvent.UPDATED, OnRoomUpdated);
            }
        }
        protected override void OnChangeClick(bool applied)
        {
            _overlayManager.LockScreen();
            ChangeAppliedDecor(applied)
                    .Then(s => {
                        if (s) {
                            _overlayManager.UnlockScreen();
                            return;
                        }
                        HandleErrorChangeAppliedItem(applied, null);
                    })
                    .Catch(e => { HandleErrorChangeAppliedItem(applied, e); });
        }
        protected override IPromise<string> Buy()
        {
            return _shopService.BuyDecor(_descriptor.Id);
        }
        private IPromise<bool> ChangeAppliedDecor(bool applied)
        {
            return applied ? _roomService.AddDecor(_descriptor.Id) : _roomService.RemoveDecor(_descriptor.Id);
        }
        private void OnRoomUpdated(RoomEvent evn)
        {
            Applied = IsApplied();
        }

        protected override bool IsApplied()
        {
            return _roomService.HasDecor(_descriptor.Id);
        }
    }
}