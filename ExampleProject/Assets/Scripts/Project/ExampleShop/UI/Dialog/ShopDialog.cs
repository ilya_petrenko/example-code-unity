﻿using System.Collections.Generic;
using AgkUI.Binding.Attributes;
using AgkUI.Binding.Attributes.Method;
using AgkUI.Core.Model;
using AgkUI.Core.Service;
using AgkUI.Dialog.Attributes;
using AgkUI.Dialog.Service;
using AgkUI.Element.Tab;
using IoC.Attribute;
using JetBrains.Annotations;
using UnityEngine;

namespace Project.ExampleShop.UI.Dialog
{
    [UIController(PREFAB)]
    [UIDialogFog(FogPrefabs.EMBEDED_SHADOW_FOG)]
    public class ShopDialog : MonoBehaviour
    {
        private const string PREFAB = "UI/Shop/Dialog/pfShopDialog@embeded";
                
        public const string BUTTONS_TAB = "Buttons";
        public const string BUTTONS_CONTAINER = "ButtonsContainer";
        private const string CATEGORY_TAB_GROUP = "TabContainer";
        private const string SHOP_CATEGORY_NAME = "ShopCategory";

        [UIComponentBinding(BUTTONS_CONTAINER, true)]
        private BuyButtonsItemsPanel _buyButtonsContainer;
        [PublicAPI]
        [UIComponentBinding(ResourcePanel.PANEL_NAME, true)]
        private ResourcePanel _resourcePanel;
        [UIComponentBinding(CATEGORY_TAB_GROUP)]
        private TabGroup _tabGroup;
        [UIObjectBinding(CATEGORY_TAB_GROUP)]
        private GameObject _tabsContainer;
        [UIObjectBinding("ContentContainer")]
        private GameObject _contentContainer;
        [Inject]
        private DialogManager _dialogManager;
        [Inject]
        private UIService _uiService;
        [Inject]
        private ShopDescriptor _shopDescriptor;
        
        private string _currentTab;
        private string _initTab;
        private bool _onlyMoney;
        private readonly List<ShopCategoryPanel> _categoryPanels = new List<ShopCategoryPanel>();

        [UICreated]
        private void Init(string initTab, bool onlyMoney)
        {
            _initTab = initTab;
            _onlyMoney = onlyMoney;
            if (onlyMoney) {
                ConfigureButtonsContent();
                return;
            }
            CreateShopCategoryPanels();
        }
        private void CreateShopCategoryPanels()
        {
            List<ShopCategoryDescriptor> shopCategoryDescriptors = _shopDescriptor.ShopCategoryDescriptors;
            _uiService.Create(UiCollectionModel.Create<ShopCategoryPanel>(shopCategoryDescriptors)
                                               .Container(_contentContainer)
                                               .Name<ShopCategoryDescriptor>(s => s.Name + SHOP_CATEGORY_NAME)
                                               .LoadItemCallback<ShopCategoryPanel>((controller) => {
                                                   _categoryPanels.Add(controller);
                                                   controller.gameObject.SetActive(false);
                                                   CreateTab(controller.name);
                                               })
                                               .LoadAllCallback(ConfigureButtonsContent))
                      .Done();
        }
        private void CreateTab(string tabName)
        {
            _uiService.Create(UiModel.Create<ShopTabButton>(tabName)
                                     .Container(_tabsContainer)
                                     .LoadCallback(o => {
                                         _tabGroup.AddTab(o.gameObject, tabName == _initTab);
                                     }))
                      .Done();
        }
        private void ConfigureButtonsContent()
        {
            _buyButtonsContainer.gameObject.SetActive(false);
            CreateTab(BUTTONS_TAB);
        }
        [UIOnTab(CATEGORY_TAB_GROUP)]
        private void OnTabSelected(TabElement tab)
        {
            CurrentTab = tab.name;
        }

        [UIOnClick("CloseButton")]
        private void OnClose()
        {
            Hide();
        }
        private void Hide()
        {
            _dialogManager.Hide(gameObject);
        }
        
        private void UpdateContent(string tabName)
        {
            _buyButtonsContainer.gameObject.SetActive(tabName == BUTTONS_TAB);
            _categoryPanels.ForEach(p => {
                p.gameObject.SetActive(p.name == tabName);
            });
        }

        public bool OnlyMoney
        {
            get { return _onlyMoney; }
        }
        private string CurrentTab
        {
            set
            { 
                if (_currentTab == value) {
                    return;
                }
                _currentTab = value;
                UpdateContent(_currentTab);
                _tabGroup.ToggleTabByName(_currentTab);
            }
        }
    }
}