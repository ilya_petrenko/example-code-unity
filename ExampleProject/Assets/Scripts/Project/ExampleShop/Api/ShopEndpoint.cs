using AgkCommons.CodeStyle;
using IoC.Attribute;
using RSG;
using WebApi;

namespace Project.ExampleShop.Api
{
    [Injectable]
    public class ShopEndpoint
    {
        private const int MAX_RETRY_COUNT = 5;
        [Inject]
        private ApiClient _client;

        public IPromise<bool> BuySkin(string itemId)
        {
            ApiRequest request = ApiRequest.Create("shop/buySkin")
                                           .AddParam("itemId", itemId)
                                           .MaxRetryCount(MAX_RETRY_COUNT)
                                           .Build();
            return _client.Request<bool>(request);
        }
        
        public IPromise<bool> BuyDecor(string itemId)
        {
            ApiRequest request = ApiRequest.Create("shop/buyDecor")
                                           .AddParam("itemId", itemId)
                                           .MaxRetryCount(MAX_RETRY_COUNT)
                                           .Build();
            return _client.Request<bool>(request);
        }
        public IPromise<bool> BuyLife(string lifeId)
        {
            ApiRequest request = ApiRequest.Create("shop/buyLife")
                                           .AddParam("lifeId", lifeId)
                                           .MaxRetryCount(MAX_RETRY_COUNT)
                                           .Build();
            return _client.Request<bool>(request);
        }
    }
}