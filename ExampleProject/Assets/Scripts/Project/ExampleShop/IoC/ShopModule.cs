﻿using IoC.Api;

namespace Project.ExampleShop.IoC
{
    public class ShopModule : IIoCModule
    {
        public void Configure(IIoCContainer container)
        {
            container.RegisterSingleton<ButtonsShopService>(); 
            container.RegisterSingleton<ShopService>();     
            container.RegisterSingleton<ShopEndpoint>();
        }
    }
}