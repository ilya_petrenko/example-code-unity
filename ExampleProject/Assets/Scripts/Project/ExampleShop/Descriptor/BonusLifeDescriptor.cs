﻿using System.Xml.Serialization;

namespace Project.ExampleShop.Descriptor
{
    [XmlRoot("bonusLife")]
    public class BonusLifeDescriptor
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("price")]
        public int Price { get; set; }
        
    }
}