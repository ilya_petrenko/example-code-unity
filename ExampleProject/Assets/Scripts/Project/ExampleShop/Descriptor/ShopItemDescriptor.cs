﻿using System.Xml.Serialization;

namespace Project.ExampleShop.Descriptor
{
    public class ShopItemDescriptor
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("type")]
        public InventoryType Type { get; set; }

        [XmlAttribute("price")]
        public int Price { get; set; }
        
        [XmlAttribute("icon")]
        public string Icon { get; set; }
        
        [XmlAttribute("name")]
        public string Name { get; set; }
        
        [XmlAttribute("description")]
        public string Description { get; set; }
        
        [XmlAttribute("descriptionLock")]
        public string DescriptionLock { get; set; }
        
        [XmlElement("visibility")]
        public VisibilityDescriptor Visibility { get; set; }
        
        [XmlElement("availability")]
        public AvailabilityDescriptor Availability { get; set; }
        
    }
}