﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Project.ExampleShop.Descriptor
{
    
    public class ShopCategoryDescriptor
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
        
        [XmlElement("shopItem")]
        public List<ShopItemDescriptor> ShopItemDescriptors { get; set; }

        public ShopItemDescriptor GetShopItem(string itemId)
        {
            return ShopItemDescriptors.FirstOrDefault(x => x.Id == itemId);
        }
    }
}