﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Project.ExampleShop.Descriptor
{
    
    public class VisibilityDescriptor
    {
        [XmlElement("condition")]
        public List<ConditionDescriptor> Conditions { get; set; }
        
    }
}