﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Project.ExampleShop.Descriptor
{
    
    public class AvailabilityDescriptor
    {
        [XmlElement("condition")]
        public List<ConditionDescriptor> Conditions { get; set; }
        
    }
}