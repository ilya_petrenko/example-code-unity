﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using JetBrains.Annotations;

namespace Project.ExampleShop.Descriptor
{
    [XmlRoot("shop")]
    public class ShopDescriptor
    {
        [XmlElement("shopCategory")]
        public List<ShopCategoryDescriptor> ShopCategoryDescriptors { get; set; }
        
        public ShopItemDescriptor RequireShopItem(string itemId)
        {
            ShopItemDescriptor shopItemDescriptor = GetShopItems().FirstOrDefault(x => x.Id == itemId);
            if (shopItemDescriptor == null) {
                throw new NullReferenceException($"ShopItemDescriptor with id= {itemId} of collection not found");
            }
            return shopItemDescriptor;
        }
        [CanBeNull]
        public ShopItemDescriptor GetShopItem(string itemId)
        {
            return GetShopItems().FirstOrDefault(x => x.Id == itemId);
        }
        public List<ShopItemDescriptor> GetShopItemsByShopCategory(string shopCategory)
        {
            ShopCategoryDescriptor shopCategoryDescriptor = ShopCategoryDescriptors.FirstOrDefault(s => s.Name == shopCategory);
            return shopCategoryDescriptor == null ? new List<ShopItemDescriptor>() : shopCategoryDescriptor.ShopItemDescriptors;
        }
        private List<ShopItemDescriptor> GetShopItems()
        {
            List<ShopItemDescriptor> shopItemDescriptors = new List<ShopItemDescriptor>();
            ShopCategoryDescriptors.ForEach(s => {
                foreach (ShopItemDescriptor shopItem in s.ShopItemDescriptors) {
                    shopItemDescriptors.Add(shopItem);
                }
            });
            return shopItemDescriptors;
        }
    }
}