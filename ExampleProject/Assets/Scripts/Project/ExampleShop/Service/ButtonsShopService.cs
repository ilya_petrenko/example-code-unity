﻿using AgkCommons.CodeStyle;
using AgkCommons.Event;
using IoC.Attribute;
using IoC.Util;
using RSG;

namespace Project.ExampleShop.Service
{
    [Injectable]
    public class ButtonsShopService : GameEventDispatcher
    {
        [Inject]
        private DescriptorCollection<ButtonsPackDescriptor> _buttonsPackDescriptors;
        [Inject]
        private IoCProvider<PurchaseService> _purchaseService;
        [Inject]
        private MoneyService _moneyService;
        public IPromise BuyButtonsPack(string buttonsPackId)
        {
            ButtonsPackDescriptor buttonsPackDescriptor = _buttonsPackDescriptors.Require(buttonsPackId);
            return _purchaseService.Require().Buy(buttonsPackDescriptor.ProductId)
                                   .Then(productId => {
                                       _moneyService.Add(buttonsPackDescriptor.FullPrice);
                                       Dispatch(new ButtonsShopEvent(ButtonsShopEvent.BUTTON_BOUGHT));
                                   });
        }
    }
}