﻿using System;
using Adept.Logger;
using AgkCommons.CodeStyle;
using AgkCommons.Event;
using IoC.Attribute;
using JetBrains.Annotations;
using RSG;

namespace Project.ExampleShop.Service
{
    [Injectable]
    public class ShopService : GameEventDispatcher
    {
        private static readonly IAdeptLogger _logger = LoggerFactory.GetLogger<ShopService>();
        [Inject]
        private ShopDescriptor _shopDescriptor;
        [Inject]
        private MoneyService _moneyService;
        [Inject]
        private InventoryService _inventoryService;
        [Inject]
        private BonusLifeDescriptor _bonusLifeDescriptor;
        [Inject]
        private ShopEndpoint _shopEndpoint;
        [Inject]
        private RoomService _roomService;
        [Inject]
        private SkinService _skinService;
       
        [PublicAPI]
        public IPromise<string> BuySkin(string itemId)
        {
            ShopItemDescriptor shopItemDescriptor = _shopDescriptor.GetShopItem(itemId);
            if (shopItemDescriptor == null) {
                return Promise<string>.Rejected(new NullReferenceException("BonusLifeDescriptor not found, itemId= " + itemId));
            }
            return _shopEndpoint.BuySkin(itemId)
                                .Then(s => {
                                    if (!s) {
                                        throw new Exception("can't buy itemId=" + itemId);
                                    }
                                    _moneyService.Sub(shopItemDescriptor.Price);
                                    _inventoryService.AddInventory(itemId);
                                    _skinService.ChangeSkin(itemId);
                                    return itemId;
                                });

        }

        [PublicAPI]
        public IPromise<string> BuyDecor(string itemId)
        {
            ShopItemDescriptor shopItemDescriptor = _shopDescriptor.GetShopItem(itemId);
            if (shopItemDescriptor == null) {
                return Promise<string>.Rejected(new NullReferenceException("BonusLifeDescriptor not found, itemId= " + itemId));
            }
            return _shopEndpoint.BuyDecor(itemId)
                                .Then(s => {
                                    if (!s) {
                                        throw new Exception("can't buy itemId=" + itemId);
                                    }
                                    _moneyService.Sub(shopItemDescriptor.Price);
                                    _inventoryService.AddInventory(itemId); 
                                    _roomService.AddDecor(itemId);
                                    return itemId;
                                });

        }
        public IPromise BuyLife()
        {
            return _shopEndpoint.BuyLife(_bonusLifeDescriptor.Id)
                                .Then(s => {
                                    if (!s) {
                                        throw new Exception("can't buy bonusLife");
                                    }
                                    _moneyService.Sub(_bonusLifeDescriptor.Price);
                                });
        }
    }
}