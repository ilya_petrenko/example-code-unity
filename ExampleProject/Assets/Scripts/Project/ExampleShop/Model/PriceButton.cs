﻿using AgkUI.Element.Text;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Project.ExampleShop.Model
{
    public class PriceButton : MonoBehaviour
    {
        [PublicAPI]
        public const string CURRENCY_ICON_PATTERN = "UI/Shop/Texture/Currency/tx{0}CurrencyIcon@embeded";
        
        [SerializeField]
        private Image _icon;
        [SerializeField]
        private UILabel _label;
        [SerializeField]
        private UILabel _currencyText;
        [SerializeField]
        private Sprite _buttonsIcon;

        [PublicAPI]
        public void SetCurrency([CanBeNull] Sprite currencyIcon, [CanBeNull] string currencyText)
        {
            if (currencyIcon != null) {
                Icon = currencyIcon;
                CurrencyText = null;
            } else {
                Icon = null;
                CurrencyText = currencyText;
            }
        }

        [PublicAPI]
        public void SetCurrency(CurrencyType currencyType, [CanBeNull] string currencyText)
        {
            Sprite sprite = GetTexture(currencyType);
            SetCurrency(sprite, currencyText);
        }

        [CanBeNull]
        private Sprite GetTexture(CurrencyType value)
        {
            return value == CurrencyType.BUTTONS ? _buttonsIcon : null;
        }

        [PublicAPI]
        public string Price
        {
            set { _label.StyledText = value; }
        }

        [PublicAPI]
        public bool Interactable
        {
            get { return GetComponent<Button>().interactable; }
            set { GetComponent<Button>().interactable = value; }
        }

        [CanBeNull]
        private string CurrencyText
        {
            set
            {
                if (_currencyText == null) {
                    return;
                }
                if (string.IsNullOrEmpty(value)) {
                    _currencyText.gameObject.SetActive(false);
                    return;
                }
                _currencyText.StyledText = value;
            }
        }

        [CanBeNull]
        private Sprite Icon
        {
            set
            {
                if (_icon == null || _icon.gameObject == null) {
                    return;
                }
                _icon.gameObject.SetActive(value != null);
                if (value != null) {
                    _icon.sprite = value;
                }
            }
        }

        public enum CurrencyType
        {
            [UsedImplicitly]
            UNKNOWN,
            [UsedImplicitly]
            BUTTONS,
            [UsedImplicitly]
            CURRENCY,
        }
    }
}