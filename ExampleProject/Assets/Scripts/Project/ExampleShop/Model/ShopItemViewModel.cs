﻿
namespace Project.ExampleShop.Model
{
    public class ShopItemViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        public string DescriptionLock { get; set; }
        public string ItemIcon { get; set; }
        public int Price { get; set; }
        public PriceButton.CurrencyType Currency { get; set; }
        
        public bool Visibility { get; set; }
        public bool Availability { get; set; }
        
        public bool Bought { get; set; }
        public bool Applied { get; set; }
    }
}