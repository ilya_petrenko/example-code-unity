﻿using JetBrains.Annotations;

namespace Project.ExampleShop.Model
{
    public class ButtonsItemViewModel
    {
        public string Name { get; set; }
        public string PromoText { get; set; }
        public string ButtonsIcon { get; set; }
        [CanBeNull]
        public string CurrencyIcon { get; set; }
        [CanBeNull]
        public string CurrencyText { get; set; }
        public int Amount { get; set; }
        public int BonusAmount { get; set; }
        public float Price { get; set; }
        public PriceButton.CurrencyType Currency { get; set; }
        public bool Selected { get; set; }
        public bool Interactable { get; set; }
    }
}